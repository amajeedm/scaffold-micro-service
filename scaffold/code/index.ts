import * as dotenv from 'dotenv';
import express from 'express';
import { json } from 'body-parser';

dotenv.config();
const serviceName = process.env.SERVICE_NAME || 'Unconfigured Service'
const servicePort = process.env.SERVICE_PORT || 8080

const app = express();
app.use(json());

app.listen(servicePort, () => {
    console.log(`${serviceName} Listening on ${servicePort}`)
})