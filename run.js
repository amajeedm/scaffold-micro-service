#!/usr/bin/env node
const inquirer = require("inquirer");
const { exec } = require("child_process");
const path = require('path');
const { unlinkSync } = require('fs')
const { writeFile, readdir, readFile } = require("fs").promises;
const { exit } = require("process");

const scaffoldFiles = {};
const scaffoldFolderPath = path.resolve(__dirname, 'scaffold');



(async () => {
    const args = process.argv.slice(2)
    // args.forEach((val, index) => {
    //     console.log(`${index}: ${val}`)
    // })

    const defaultServiceName = process.cwd().split(path.sep).pop();
    const defaultServicePort = '8080'

    const { serviceName } = await inquirer.prompt([
        {
            type: "input",
            message: `Service name: `,
            name: "serviceName",
            default: defaultServiceName,
        }
    ]);

    const { servicePort } = await inquirer.prompt([
        {
            type: "input",
            message: `Service port: `,
            name: "servicePort",
            default: defaultServicePort,
        }
    ]);

    const { dockerAccount } = await inquirer.prompt([
        {
            type: "input",
            message: `Docker ID (to be used for k8s image name): `,
            name: "dockerAccount",
            default: "your-docker-id",
        }
    ]);

    const serviceNamePlaceHolder = '{SERVICE_NAME}';
    const servicePortPlaceHolder = '{SERVICE_PORT}';
    const dockerAccountPlaceHolder = '{DOCKER_ACCOUNT}';

    const files = await readdir(scaffoldFolderPath).catch(console.log);
    const alwaysCreateFiles = ['k8s.yaml']

    console.log("**********Started**********");
    console.log("# Unpacking and injecting variables");
    for (let file of files) {
        console.log(file)
        let fileContent = ""
        if (!alwaysCreateFiles.includes(file)) {
            console.log(`[ ] ${file}`)
            fileContent = await readFile(path.join(process.cwd(), file)).catch((error) => { })
        } else {
            console.log(`[+] ${file}`)
        }
        fileContent = fileContent || await readFile(path.join(scaffoldFolderPath, file)).catch((error) => {
            console.log(error);
            exit(1)
        });

        //Search and replace placeholders
        fileContent = fileContent.toString().replace(new RegExp(serviceNamePlaceHolder, 'g'), serviceName);
        fileContent = fileContent.replace(new RegExp(servicePortPlaceHolder, 'g'), servicePort);
        fileContent = fileContent.replace(new RegExp(dockerAccountPlaceHolder, 'g'), dockerAccount);

        const fileToWrite = path.join(process.cwd(), file);
        //console.log(fileContent);
        await writeFile(fileToWrite, fileContent.toString()).catch((error) => { console.log(`Skipping ${file}`) });
    }
    console.log("-");

    const npmInstall = `cd ${process.cwd()}; npm install express @types/express typescript ts-node-dev dotenv`;
    console.log(`#Installing npm packages`)
    exec(npmInstall, (error, stdout, stderr) => {
        if (!error) {
            console.log(`[ ] Updated npm packages`)
        } else if (stderr) {
            console.error(`Error: Updating npm packages`)
        } else {
            console.error(`Error: Updating npm packages`)
        }

        console.log("-");
        console.log("*********Finished**********");
        exit();
    })
})();